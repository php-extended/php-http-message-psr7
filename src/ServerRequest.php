<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\ServerRequestInterface;
use Stringable;

/**
 * ServerRequest class file.
 *
 * This class is a simple implementation of the ServerRequestInterface.
 *
 * @author Anastaszor
 */
class ServerRequest extends Request implements ServerRequestInterface, Stringable
{
	
	/**
	 * The cookies which will be used to process the request.
	 *
	 * @var array<string, string>
	 */
	protected array $_cookies = [];
	
	/**
	 * The query params which will be used to process the request.
	 *
	 * @var array<string, string>
	 */
	protected array $_query = [];
	
	/**
	 * The query body which will be used to process the request.
	 *
	 * @var array<string, string>
	 */
	protected array $_sbody = [];
	
	/**
	 * The uploaded files which will be used to process the request.
	 *
	 * @var array<string, \Psr\Http\Message\UploadedFileInterface>
	 */
	protected array $_files = [];
	
	/**
	 * The additional attributes of this request.
	 *
	 * @var array<string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 */
	protected array $_attributes = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getServerParams()
	 * @return array<string, string>
	 * @SuppressWarnings("PHPMD.Superglobals")
	 */
	public function getServerParams() : array
	{
		global $argv, $argc;
		
		$params = [];
		$params = \array_merge($params, $_SERVER);
		$params = \array_merge($params, $_ENV);
		
		/** @psalm-suppress RedundantCondition */
		if(isset($argv) && !empty($argv))
		{
			$params['argv'] = $argv;
		}
		
		/** @psalm-suppress RedundantCondition */
		if(isset($argc) && !empty($argc))
		{
			$params['argc'] = $argc;
		}
		
		$return = [];
		
		foreach($params as $key => $value)
		{
			/** @psalm-suppress PossiblyInvalidCast */
			$return[(string) $key] = (string) $value;
		}
		
		return $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getCookieParams()
	 * @return array<string, string>
	 */
	public function getCookieParams() : array
	{
		return $this->_cookies;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withCookieParams()
	 * @param array<string, string> $cookies
	 * @return static
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function withCookieParams(array $cookies) : ServerRequestInterface
	{
		$newobj = clone $this;
		$newobj->_cookies = \array_merge($this->_cookies, $cookies);
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getQueryParams()
	 * @return array<string, string>
	 */
	public function getQueryParams() : array
	{
		return $this->_query;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withQueryParams()
	 * @param array<string, string> $query
	 * @return static
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function withQueryParams(array $query) : ServerRequestInterface
	{
		$newobj = clone $this;
		$newobj->_query = $query;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getUploadedFiles()
	 * @return array<string, \Psr\Http\Message\UploadedFileInterface>
	 */
	public function getUploadedFiles() : array
	{
		return $this->_files;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withUploadedFiles()
	 * @param array<string, \Psr\Http\Message\UploadedFileInterface> $uploadedFiles
	 * @return static
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function withUploadedFiles(array $uploadedFiles) : ServerRequestInterface
	{
		$newobj = clone $this;
		$newobj->_files = \array_merge_recursive($this->_files, $uploadedFiles);
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getParsedBody()
	 * @return array<string, string>
	 */
	public function getParsedBody()
	{
		return $this->_sbody;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withParsedBody()
	 * @param array<string, string> $data
	 * @return static
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function withParsedBody($data) : ServerRequestInterface
	{
		$newobj = clone $this;
		$newobj->_sbody = $data;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getAttributes()
	 * @return array<string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 */
	public function getAttributes() : array
	{
		return $this->_attributes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getAttribute()
	 */
	public function getAttribute(string $name, $default = null)
	{
		if(isset($this->_attributes[$name]))
		{
			return $this->_attributes[$name];
		}
		
		return $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withAttribute()
	 */
	public function withAttribute(string $name, $value) : ServerRequestInterface
	{
		$newobj = clone $this;
		/** @psalm-suppress MixedPropertyTypeCoercion */
		$newobj->_attributes[$name] = $value;
		
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withoutAttribute()
	 */
	public function withoutAttribute(string $name) : ServerRequestInterface
	{
		if(!isset($this->_attributes[$name]))
		{
			return $this;
		}
		
		$newobj = clone $this;
		unset($newobj->_attributes[$name]);
		
		return $newobj;
	}
	
}
