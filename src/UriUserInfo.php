<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpMessage;

use Stringable;

/**
 * UriUserInfo class file.
 * 
 * This class represents only the user info part of the url.
 * 
 * @author Anastaszor
 */
class UriUserInfo implements Stringable
{
	
	/**
	 * The user name of the authority.
	 *
	 * @var ?string
	 */
	protected ?string $_username = null;
	
	/**
	 * The user password of the authority.
	 *
	 * @var ?string
	 */
	protected ?string $_password = null;
	
	/**
	 * Builds a new UriAuthority with its inner data.
	 * 
	 * @param string $username
	 * @param string $password
	 */
	public function __construct(?string $username = null, ?string $password = null)
	{
		if(null !== $username && '' !== $username)
		{
			$this->_username = \str_replace('+', ' ', \rawurldecode($username));
			
			if(null !== $password && '' !== $password)
			{
				$password = \str_replace('+', ' ', \rawurldecode($password));
			}
			
			$this->_password = $password;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		if(null === $this->_username || '' === $this->_username)
		{
			return '';
		}
		
		$str = \rawurlencode($this->_username);
		
		if(null !== $this->_password && '' !== $this->_password)
		{
			$str .= ':'.\rawurlencode($this->_password);
		}
		
		return $str;
	}
	
	/**
	 * Gets the username of the user info.
	 * 
	 * @return ?string
	 */
	public function getUsername() : ?string
	{
		return $this->_username;
	}
	
	/**
	 * Gets the password of the user info.
	 * 
	 * @return ?string
	 */
	public function getPassword() : ?string
	{
		return $this->_password;
	}
	
}
