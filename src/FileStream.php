<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpMessage;

use Exception;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use Stringable;

/**
 * FileStream class file.
 *
 * This class represents a read and write stream implementation of
 * the StreamInterface which relies on underlying files.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @psalm-suppress PropertyNotSetInConstructor
 */
class FileStream implements StreamInterface, Stringable
{
	
	/**
	 * The target path of the file.
	 *
	 * @var string
	 */
	protected string $_path;
	
	/**
	 * The handle of the file at target path.
	 *
	 * @var ?resource
	 */
	protected $_handle;
	
	/**
	 * The total length of the file.
	 *
	 * @var integer
	 */
	protected int $_fileLength;
	
	/**
	 * Whether the stream is in detached state. In detached state, the
	 * stream is unusable.
	 *
	 * @var boolean
	 */
	protected bool $_detached = false;
	
	/**
	 * Builds a new FileStream object with the given target path.
	 *
	 * @param string|resource $pathOrResource
	 * @param ?integer $fileLength the length of the stream
	 * @throws RuntimeException if the file does not exists at the given path
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function __construct($pathOrResource, ?int $fileLength = null)
	{
		if(null !== $fileLength)
		{
			$this->_fileLength = $fileLength;
		}
		
		if(\is_string($pathOrResource))
		{
			$realpath = \realpath($pathOrResource);
			if(false === $realpath)
			{
				$message = 'The file path does not point to an existing file.';
				$context = ['{path}' => $pathOrResource];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			if(!\is_file($realpath))
			{
				$message = 'The file at path "{path}" is not a file.';
				$context = ['{path}' => $realpath];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$this->_path = $realpath;
			if(empty($this->_fileLength) && null === $fileLength)
			{
				$this->_fileLength = (int) \filesize($this->_path);
			}
		}
		
		if(\is_resource($pathOrResource))
		{
			$this->_handle = $pathOrResource;
			/** @psalm-suppress PossiblyInvalidCast */
			$this->_path = (string) $this->getMetadata('uri');
			if(null === $fileLength)
			{
				if(empty($this->_fileLength))
				{
					$this->_fileLength = (int) \filesize($this->_path);
				}
				if(empty($this->_fileLength))
				{
					/** @psalm-suppress RiskyCast */
					$this->_fileLength = (int) $this->getMetadata('unread_bytes');
				}
				if(empty($this->_fileLength))
				{
					$stat = \fstat($this->_handle);
					$this->_fileLength = (int) ($stat['size'] ?? 0);
				}
			}
		}
		
		$this->_detached = false;
		$this->rewind();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::__toString()
	 */
	public function __toString() : string
	{
		if($this->_detached)
		{
			return '';
		}
		
		if(null === $this->_handle)
		{
			$res = \file_get_contents($this->_path);
			if(false === $res)
			{
				return '';
			}
			
			return $res;
		}
		
		try
		{
			$this->rewind();
			
			return $this->read((int) $this->getSize());
		}
		catch(Exception $e)
		{
			return '';
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::close()
	 */
	public function close() : void
	{
		if(null !== $this->_handle)
		{
			\flock($this->_handle, \LOCK_UN);
			/** @psalm-suppress InvalidPropertyAssignmentValue */
			\fclose($this->_handle);
			$this->_handle = null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::detach()
	 * @return null|resource
	 */
	public function detach()
	{
		$this->_detached = true;
		$this->close();
		
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getSize()
	 */
	public function getSize() : ?int
	{
		return $this->_fileLength;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::tell()
	 */
	public function tell() : int
	{
		if($this->_detached || null === $this->_handle)
		{
			return 0;
		}
		
		$res = false;
		if(null !== $this->_handle)
		{
			$res = \ftell($this->_handle);
		}
		if(false !== $res)
		{
			return $res;
		}
		
		throw new RuntimeException('Impossible to tell the stream position.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::eof()
	 */
	public function eof() : bool
	{
		if($this->_detached || null === $this->_handle)
		{
			return true;
		}
		
		try
		{
			return $this->tell() === $this->getSize();
		}
		catch(RuntimeException $e)
		{
			return true;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isSeekable()
	 */
	public function isSeekable() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::seek()
	 * @param integer $offset
	 * @param integer $whence
	 * @throws RuntimeException
	 */
	public function seek(int $offset, int $whence = \SEEK_SET) : void
	{
		$this->ensureStream();
		$res = -1;
		if(null !== $this->_handle)
		{
			$res = \fseek($this->_handle, $offset, $whence);
		}
		if(-1 === $res)
		{
			throw new RuntimeException('Impossible to seek the file.');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::rewind()
	 */
	public function rewind() : void
	{
		$this->seek(0, \SEEK_SET);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isWritable()
	 */
	public function isWritable() : bool
	{
		return \is_writable($this->_path);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::write()
	 */
	public function write(string $string) : int
	{
		$this->ensureStream();
		$res = false;
		if(null !== $this->_handle)
		{
			$res = \fwrite($this->_handle, $string);
		}
		if(false !== $res)
		{
			return $res;
		}
		// calculate the new length given the emplacement we're in the stream
		$this->_fileLength = \max($this->_fileLength, $this->tell() + (int) \mb_strlen($string, '8bit'));
		
		throw new RuntimeException('Impossible to write to file.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isReadable()
	 */
	public function isReadable() : bool
	{
		return \is_readable($this->_path);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::read()
	 */
	public function read(int $length) : string
	{
		$this->ensureStream();
		if(0 >= $length)
		{
			return '';
		}
		$res = false;
		if(null !== $this->_handle)
		{
			$res = \fread($this->_handle, $length);
		}
		if(false === $res)
		{
			$message = 'Impossible to read the next {n} bytes.';
			$context = ['{n}' => $length];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getContents()
	 */
	public function getContents() : string
	{
		$this->ensureStream();
		
		return $this->read((int) $this->getSize());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getMetadata()
	 * @param ?string $key
	 */
	public function getMetadata(?string $key = null)
	{
		try
		{
			$this->ensureStream();
		}
		catch(RuntimeException $e)
		{
			return null;
		}
		
		$smd = null;
		if(null !== $this->_handle)
		{
			$smd = \stream_get_meta_data($this->_handle);
		}
		if(null === $key)
		{
			return $smd;
		}
		if(isset($smd[$key]))
		{
			return $smd[$key];
		}
		
		return null;
	}
	
	/**
	 * Ensures that the handle exists for this stream.
	 *
	 * @throws RuntimeException if the handle cannot be opened or locked
	 */
	protected function ensureStream() : void
	{
		if($this->_detached)
		{
			throw new RuntimeException('The stream is in detached state.');
		}
		
		if(null === $this->_handle)
		{
			if(!\is_file($this->_path))
			{
				$message = 'The file at path "{path}" is not a file.';
				$context = ['{path}' => $this->_path];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$handle = \fopen($this->_path, 'c+');
			if(false === $handle)
			{
				$message = 'Impossible to open file {path}.';
				$context = ['{path}' => $this->_path];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			
			$this->_handle = $handle;
			if(!\flock($this->_handle, \LOCK_EX))
			{
				$message = 'Impossible to lock file {path}.';
				$context = ['{path}' => $this->_path];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
	}
	
	/**
	 * Closes the stream at destruction of this object.
	 */
	public function __destruct()
	{
		$this->detach();
	}
	
}
