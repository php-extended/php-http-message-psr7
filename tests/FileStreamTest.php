<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\FileStream;
use PHPUnit\Framework\TestCase;

/**
 * FileStreamTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpMessage\FileStream
 *
 * @internal
 *
 * @small
 */
class FileStreamTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileStream
	 */
	protected FileStream $_object;
	
	public function testToString() : void
	{
		$read = $this->_object->__toString();
		
		$this->assertGreaterThan(100, \mb_strlen($read));
	}
	
	public function testClose() : void
	{
		$this->_object->close();
		
		$this->assertTrue($this->_object->eof());
	}
	
	public function testDetach() : void
	{
		$this->expectException(RuntimeException::class);
		
		$this->_object->detach();
		
		$this->_object->getContents();
	}
	
	public function testGetSize() : void
	{
		$this->assertGreaterThanOrEqual(1, $this->_object->getSize());
	}
	
	public function testTell() : void
	{
		$this->_object->read(10);
		$this->assertGreaterThan(1, $this->_object->tell());
	}
	
	public function testEof() : void
	{
		$this->assertFalse($this->_object->eof());
	}
	
	public function testIsSeekable() : void
	{
		$this->assertTrue($this->_object->isSeekable());
	}
	
	public function testSeek() : void
	{
		$this->_object->seek(100);
		
		$this->assertEquals(100, $this->_object->tell());
	}
	
	public function testRewind() : void
	{
		$this->_object->rewind();
		
		$this->assertEquals(0, $this->_object->tell());
	}
	
	public function testIsWriteable() : void
	{
		$this->assertTrue($this->_object->isWritable());
	}
	
	public function testIsReadable() : void
	{
		$this->assertTrue($this->_object->isReadable());
	}
	
	public function testRead() : void
	{
		$read = $this->_object->read(100);
		
		$this->assertEquals(100, \mb_strlen($read));
	}
	
	public function testGetContents() : void
	{
		$read = $this->_object->getContents();
		
		$this->assertGreaterThan(100, \mb_strlen($read));
	}
	
	public function testGetMetadata() : void
	{
		$md = $this->_object->getMetadata();
		
		$this->assertIsArray($md);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileStream(__FILE__);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object->close();
	}
	
}
