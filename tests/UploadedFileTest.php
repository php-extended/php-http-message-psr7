<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\StringStream;
use PhpExtended\HttpMessage\UploadedFile;
use PHPUnit\Framework\TestCase;

/**
 * UploadedFileTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpMessage\UploadedFile
 *
 * @internal
 *
 * @small
 */
class UploadedFileTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UploadedFile
	 */
	protected UploadedFile $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetStream() : void
	{
		$this->assertEquals('stringstream', $this->_object->getStream()->getContents());
	}
	
	public function testGetSize() : void
	{
		$this->assertEquals(1234, $this->_object->getSize());
	}
	
	public function testGetError() : void
	{
		$this->assertEquals(5678, $this->_object->getError());
	}
	
	public function testGetClientFilename() : void
	{
		$this->assertEquals('name', $this->_object->getClientFilename());
	}
	
	public function testGetClientMediaType() : void
	{
		$this->assertEquals('type', $this->_object->getClientMediaType());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UploadedFile('name', 'tempName', 'type', 1234, 5678, new StringStream('stringstream'));
	}
	
}
