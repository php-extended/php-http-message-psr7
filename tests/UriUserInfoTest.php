<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\UriUserInfo;
use PHPUnit\Framework\TestCase;

/**
 * UriUserInfoTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpMessage\UriUserInfo
 *
 * @internal
 *
 * @small
 */
class UriUserInfoTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UriUserInfo
	 */
	protected UriUserInfo $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('username:p%40ssword', $this->_object->__toString());
	}
	
	public function testGetUsername() : void
	{
		$this->assertEquals('username', $this->_object->getUsername());
	}
	
	public function testGetPassword() : void
	{
		$this->assertEquals('p@ssword', $this->_object->getPassword());
	}
	
	public function testEmptyUserInfo() : void
	{
		$this->assertEquals('', (new UriUserInfo())->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UriUserInfo('username', 'p@ssword');
	}
	
}
