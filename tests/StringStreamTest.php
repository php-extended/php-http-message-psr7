<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-psr7 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;

/**
 * StringStreamTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpMessage\StringStream
 *
 * @internal
 *
 * @small
 */
class StringStreamTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StringStream
	 */
	protected StringStream $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('stringdata', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StringStream('stringdata');
	}
	
}
